def encrypt(plaintext, keyword):
    ciphertext = ""
    n = 0
    for i in plaintext:
        if i.isalpha(): ciphertext += shiftAlpha(i, indexAlpha(keyword[n % len(keyword)]))
        else: ciphertext += i
        n += 1
    return ciphertext

def decrypt(ciphertext, keyword):
    plaintext = ""
    n = 0
    for i in ciphertext:
        if i.isalpha(): plaintext += shiftAlpha(i, -indexAlpha(keyword[n % len(keyword)]))
        else: plaintext += i
        n += 1
    return plaintext

def indexAlpha(letter):
    let = ord(letter)
    if (let >= ord('A') and let <= ord('Z')): let -= ord('A')
    elif (let >= ord('А') and let <= ord('Я')): let -= ord('А')
    elif (let >= ord('a') and let <= ord('z')): let -= ord('a')
    elif (let >= ord('а') and let <= ord('я')): let -= ord('а')
    else: let = -1
    return let+1
def shiftAlpha(letter, count):
    let = ord(letter)
    if (let >= ord('A') and let <= ord('Z')): let = shiftCycle(ord('A'), ord('Z'), let, count)
    elif (let >= ord('А') and let <= ord('Я')): let = shiftCycle(ord('А'), ord('Я'), let, count)
    elif (let >= ord('a') and let <= ord('z')): let = shiftCycle(ord('a'), ord('z'), let, count)
    elif (let >= ord('а') and let <= ord('я')): let = shiftCycle(ord('а'), ord('я'), let, count)      
    letter = chr(let)
    return letter
def shiftCycle(a, b, N, count):
    N += count
    if N > b: N -= b-a+1
    elif N < a: N += b-a+1
    return N
text = input('Введите открытый текст: ')
key = input('Введите ключ: ')
enc = encrypt(text, key)
print('Зашифрованный текст: ' + enc)
dec = decrypt(enc, key)
print('Расшифрованный текст: ' + dec)